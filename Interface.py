#Kevin DOS SANTOS - Interface

from tkinter import *
from Renommage import *
import os, sys

"""
Auteur: DOS SANTOS Kevin
Date de création: 30/11/2017
"""
#============================================================================#
#===============================RenFichier===================================#
#======Mon premier programme avec mon premier langage de programmation=======#
fenetre = Tk()
fenetre.title("Kevin Dos Santos - First program with my first language")

#Insertion du logo Cloud Temple
photo = PhotoImage(file="CloudTemple2.png")
"""
   Insertion du logo de Cloud Temple (Entreprise de Service Numérique by Kevin DOS SANTOS
"""
canvas = Canvas(fenetre,width=330, height=84)
canvas.create_image(0, 0, anchor=NW, image=photo)
canvas.pack()

def regles():
    """
        Fonction permettant d'intéragir avec le menu ==> Informations
    """
    messagebox.showinfo("Information","RenFichier est un logiciel de renommage de fichier qui a été fait dans le cadre du L3 M2i. N'ayant jamais appris de langage de programmation auparavant, celui-ci contient quelques bugs utilisateur")

def apropos():
    """
        Fonction permettant d'intéragir avec le menu ==> A propos
    """
    messagebox.showinfo("Information", "Ce logiciel a été codé par Kevin DOS SANTOS")

#Création de la frame principale
#def Frame1(self):
    """
        La Frame1 utilise les valeurs de borderwidth et relief pour définir sa taille
        Le Label ci-dessous permet de donner un nom à Frame1
    """
Frame1 = Frame(fenetre, borderwidth=100, relief=GROOVE)
Frame1.pack(fill=BOTH)
message = Label(Frame1, text="RenFichier", font=("Arial", 10))
message.pack(side=BOTTOM, fill=X)

#Création du Menu de RenFichier
menubar = Menu(fenetre)
menu1 = Menu(menubar, tearoff=0)
menu1.add_command(label="Informations", command=regles)
menu1.add_separator()
menu1.add_command(label="Quitter", command=fenetre.destroy)
menubar.add_cascade(label="Règles", menu=menu1)
menu2 = Menu(menubar, tearoff=0)
menu2.add_command(label="A propos", command=apropos)
menubar.add_cascade(label="?", menu=menu2)
fenetre.config(menu=menubar)

#Création du label indiquant qu'est-ce que fait ce logiciel
champ_label = Label(fenetre, text="RenFichier est un logiciel de renommage de fichier")
champ_label.pack()

#Ensemble de boutons de l'interface graphique
#bouton_lister = Button(fenetre, text="Lister", command = test3.Lister)
#bouton_lister.pack(side=LEFT, padx=5, pady=5)
bouton_lettre = Button(fenetre, text="Lettre", command = fenetre.destroy)
bouton_lettre.pack(side=LEFT, padx=5, pady=5)
bouton_chiffre = Button(fenetre, text="Chiffre", command = fenetre.destroy)
bouton_chiffre.pack(side=LEFT, padx=5, pady=5)
bouton_lettre = Button(fenetre, text="Créer", command = LISTEREGLE.load)
bouton_lettre.pack(side=LEFT, padx=5, pady=5)
bouton_lister = Button(fenetre, text="Lister", command=ACTION.Lister)
bouton_lister.pack(side=LEFT, padx=5, pady=5)
bouton_quitter = Button(fenetre, text="Quitter", command=fenetre.destroy)
bouton_quitter.pack(side=LEFT, padx=5, pady=5)

#Création des multiples textbox
var_texte = StringVar()
ligne_texte = Entry(fenetre, textvariable=var_texte, width=30)
ligne_texte.pack(side=LEFT, padx=2, pady=2)
label = Label(fenetre, text="postfixe", bg="yellow")
label.pack(side=LEFT, padx=10, pady=100)

var_texte = StringVar()
ligne_texte2 = Entry(fenetre, textvariable=var_texte, width=30)
ligne_texte2.pack(side=LEFT, padx=2, pady=2)
label = Label(fenetre, text="Nom du répertoire", bg="red")
label.pack(side=LEFT, padx=10, pady=100)

var_texte = StringVar()
ligne_texte2 = Entry(fenetre, textvariable=var_texte, width=30)
ligne_texte2.pack(side=LEFT, padx=2, pady=2)
label = Label(fenetre, text="A partir de", pady=10)


#Création d'une liste deroulante
liste = Listbox(fenetre)
liste.pack(side=TOP, padx=10, pady=10)


#Insertion de mots dans la liste déroulante
liste.insert(END, "Afichier")
liste.insert(END, "Afichier001")
liste.insert(END, "AfichierAAA")

#Création de case à cocher
var_case = IntVar()
case = Checkbutton(fenetre, text="Ne plus poser cette question", variable=var_case)
case.pack()

#Attend la fermeture de la fenetre
fenetre.mainloop()
