#Kevin DOS SANTOS - Renommage

from tkinter import *
import os, sys
from Interface import *
from tkinter import messagebox

alpha = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTVWXYZ"

#============================================================================#
#==========================Création des classes==============================#
#======Mon premier programme avec mon premier langage de programmation=======#

class REGLES:
    def __init__(self, nom = []):
        """ Création de la classe principal REGLES, avec plusieurs paramètre
            tel que amorce, apartirde,....
        """
        self.amorce = ""
        self.apartirde = ""
        self.nomFichier = False
        self.prefixe = "JKLMNOPQ"
        self.postfixe = "JKLMNOPQ"
        self.extension = ""
        self.nom = nom

#Set qui permet d'attribuer le nom d'origine ou celui modifié par l'utilisateur
#true si on prend le nom modifié et false si on prend le nom d'origine
    def set_nomFichier(self, value):
        """
            Configure le nomFichier avec les valeurs True or False
            Recupère et modifie avec la commande get_()
        """
        if value==True:
            saisi=str(input("saisissez le nom du fichier modifié : "))
        else:
            saisit="fichier d'origine"
        print("l'utilisateur a donc choisit ce nom: " + saisi)
        self.nomFichier = saisi

    """
    Getter pour l'attribut amorce qui retourne une chaine de caractère
    return String
    """
    def get_amorce(self):
        return self.amorce
    """
    Setter pour l'attribut amorce
    """
    def set_amorce(self):
        return self.amorce()
    
    """
    Getter pour l'attribut apartirde
    @return String
    """
    def get_apartirde(self):
        return self.apartirde(self)
    """
    Setter pour l'attribut apartirde
    """
    def set_apartirde(self):
        return self.apartirde()
    
    """
    Getter pour l'attribut nomFichier
    @return String
    """
    def get_nomFichier(self):
        return nomFichier(self)
    """
    Setter pour l'attribut nomFichier
    """
    def set_nomFichier(self):
        return nomFichier()
    
    """
    Getter pour l'attribut prefixe
    @return String
    """
    def get_prefixe(self):
        return self.prefixe(self)
    """
    Setter pour l'attribut prefixe
    """
    def set_prefixe(self):
        return self.prefixe()

    """
    Getter pour l'attribut extension
    @return String
    """
    def get_extension(self):
        return self.extension(self)
    """
    Setter pour l'attribut extension
    """
    def set_extension(self):
         self.extension()

    """
    Getter pour l'attribut nom
    @return String
    """
    def get_nom(self):
        return self.nom(self)
    """
    Setter pour l'attribut nom
    """
    def set_nom(self):
        return self.nom()


#Création de la classe REGLES
class LISTEREGLE:
    """
        Création de la classe LISTEREGLE qui contient les fonctions save, load, addName
    """
    
    def __init__(self, regles = ""):
        self.regles = regles

    def addRegle(self, i):
        return self.nom.append(i)

    def save(self):
        RenFichier = open("RenFichier.ini","w")
        for mot in self.nom:
              RenFichier.write(str(mot)+"\n")
              RenFichier.close()

    def load(self):
        with open("RenFichier.ini", "r") as f:
            RenFichier = [line.strip() for line in f]
            print(RenFichier)

#Création de la classe ACTION
class ACTION:
    """
        Classe qui va permettre de faire vivre les boutons grâce aux fonctions
        tel que Lister
    """
    def __init__(self, repertoire = [], Lister = ""):
      self.nomdurepertoire = repertoire
      self.Lister = ()

    def Lister():
        path = "D:\Dropbox"
        dirs = os.listdir("D:\Dropbox")
        for file in dirs:
           messagebox.showinfo("Liste des fichiers/dossier", dirs)
           fenetre.mainloop()

    def __str__(self):
        return "Afficher lister : {0}".format(self.Lister)

class RENOMMAGE(ACTION):
    """
        Classe de renommage fichier, agit avec le choix utilisateur
    """
    def __init__(self, renommage = ""):
        ACTION.__init__(self, repertoire = "", Lister = "")
        self.renommage = ""
        self.Lister = ""

    def get_renommage(self):
        return self.renommage(self)

    def __str__(self):
        return "test d'un return pour Lister qui provient de la classe Mère: {0}".format(self.Lister)

#Objets pour appeler les classes/fonctions
test1 = REGLES()
test2 = LISTEREGLE()
test3 = ACTION()
test4 = RENOMMAGE()
